﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ScenesMoveScript : MonoBehaviour
{
    //新規作成画面へ遷移
    public void NewCreateButton()
    {
        SceneManager.LoadScene("RoomCreate");
    }

    //ルーム一覧画面へ遷移
    public void RoomListButton()
    {
        SceneManager.LoadScene("RoomList");
    }

    //タイトル画面へ遷移
    public void TitleButton()
    {
        SceneManager.LoadScene("Title");
    }

    //アプリケーション終了
    public void EndButton()
    {
       Application.Quit();
    }
    
}
