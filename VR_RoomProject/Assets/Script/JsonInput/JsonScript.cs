﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class JsonData
{
    public RoomInfo[] roomInfos;
}
[System.Serializable]
public class RoomInfo
{
    public int id;
    public string name;
    public bool check;
    public int tatami;
    public double size;
    public double height;
    public int ratioX;
    public int ratioY;

}


public class JsonScript : MonoBehaviour
{
    public Text roomName;
    public Text size;
    public Text height;
    public Text ratioX;
    public Text ratioY;

    public Dropdown dropdown;
    void Start()
    {
        //ResourcesからRoomInfo.jsonを読み込み、string型にキャスト
        string json = Resources.Load<TextAsset>("RoomInfo").ToString();

        //JsonData型のインスタンスを作成
        JsonData jsonData = new JsonData();

        //jsonのデータをjsonDataに格納
        JsonUtility.FromJsonOverwrite(json, jsonData);

        //RoomList画面から選択された部屋IDを元に部屋情報を格納
        foreach(var item in jsonData.roomInfos){

            if(item.id == 0){
                roomName.text=item.name;
                dropdown.value=item.tatami;
                size.text=item.size.ToString();
                height.text=item.height.ToString();
                ratioX.text=item.ratioX.ToString();
                ratioY.text=item.ratioY.ToString();
            }
            
            Debug.Log("id: " + item.id);
            Debug.Log("name: " + item.name);

        }
    }

}
